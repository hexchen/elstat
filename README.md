# Elstat

Status page software.

## Installing

requirements:
 - crystal 0.29.0

```
git clone https://gitlab.com/elixire/elstat
cd elstat

# edit config.ini as you wish
cp config.example.ini config.ini

shards install
shards build --production

# build frontend
# check instructions on priv/frontend/README.md
cd priv/frontend
```

## Run

As of right now, it is required to run elstat on the same directory as the
folder containing the source.

```
./bin/elstat
```

## Run tests

```
crystal spec
```
