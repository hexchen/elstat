require "http"
require "uuid"

require "./context"
require "./adapter"
require "./alerts/*"
require "./worker"

require "./api/streaming"
require "./api/incidents"

# Defines the sql column information for an adapter's spec[:db]
COLUMNS = {
  "timestamp" => "timestamp bigint",
  "status"    => "status bool",
  "latency"   => "latency bigint",
}

# Maps adapter strings to Adapter subclasses
ADAPTERS = {
  "ping" => PingAdapter,
  "http" => HTTPAdapter,
}

# Maps alert strings to Alert subclasses
ALERTS = {
  "discord" => DiscordAlert,
  "twilio"  => TwilioAlert,
}

alias Service = Hash(String, String)
alias WebsocketID = UUID

enum AlertType
  None
  Error
  ErrorOk
  Slow
  SlowOk
end

# Main class for everything related to Elstat.
# Creates tables, manages websocket subscriptions, spawns service workers, etc.
class Manager
  @workers = {} of String => Fiber

  @subscribers = {} of String => Set(WebsocketID)
  @websockets = {} of WebsocketID => HTTP::WebSocket

  @main_channel = Channel(WorkerResult).new
  @state = {} of String => AdapterResult

  def initialize(@context : Context)
  end

  def context
    @context
  end

  def service_keys : Array(String)
    root = @context.cfg["elstat"]

    if root.has_key?("order")
      res = [] of String

      root["order"].split(',').each do |service_name|
        res.push("service:#{service_name}")
      end

      return res
    else
      @context.cfg.each_key.select(&.starts_with?("service:")).to_a
    end
  end

  def service_names : Array(String)
    self.service_keys.map(&.lchop("service:")).to_a
  end

  def alert_keys
    @context.cfg.each_key.select &.starts_with?("alert:")
  end

  private def column_names(service : Service) : Array(String)
    adapter_name = service["adapter"]
    adapter = ADAPTERS[adapter_name]
    return adapter.spec[:db]
  end

  def column_names_from_name(service_name : String) : Array(String)
    serv = @context.cfg["service:#{service_name}"]
    return self.column_names(serv)
  end

  def make_db_table(name : String, service : Service)
    columns = column_names(service)
      .map { |col| COLUMNS[col] }
      .join(",\n")

    @context.db.exec "
      create table if not exists #{name} (
      #{columns}
      )
    "

    @context.db.exec "
      CREATE TABLE IF NOT EXISTS incidents (
          id text PRIMARY KEY,
          incident_type text,
          title text,
          content text,
          ongoing bool,
          start_timestamp bigint,
          end_timestamp bigint
      );"

    @context.db.exec "CREATE TABLE IF NOT EXISTS incident_stages (
          parent_id text REFERENCES incidents (id) NOT NULL,
          timestamp bigint,
          title text,
          content text,
          PRIMARY KEY (parent_id, timestamp)
      );"
  end

  private def commit_result(serv_name : String, result : AdapterResult) : Int64
    columns = self.column_names_from_name(serv_name)

    columns_sql = columns.join(",")
    args_str = (["?"] * columns.size).join(",")

    stmt = "insert into #{serv_name} (#{columns_sql})
     values (#{args_str})"

    # create args
    timestamp = Time.utc.to_unix_ms.to_i64

    # TODO this only works because all our existing adapters use
    # status and latency as of right now.

    # creating a new adapter that doesn't have the same amount of
    # columns here will cause a crash.
    args = Tuple(AdapterValue, AdapterValue, AdapterValue).new(
      timestamp, result["status"], result["latency"],
    )

    @context.db.exec(stmt, *args)
    return timestamp
  end

  private def ws_chan_prefixes(serv_name : String) : Array(String)
    cols = self.column_names_from_name(serv_name)

    # always ignore the first one, which is always timestamp.
    chan_prefixes = cols[1, cols.size + 1]
    return chan_prefixes
  end

  private def dispatch_result(
    serv_name : String, timestamp : Int64, result : AdapterResult
  )
    prefixes = self.ws_chan_prefixes(serv_name)

    prefixes.each do |prefix|
      spawn self.dispatch_channel(
        "#{prefix}:#{serv_name}", timestamp, result[prefix])
    end
  end

  private def dispatch_channel(
    channel : String, timestamp : Int64, value : AdapterValue
  )
    @context.log.debug("dispatching to #{channel} : #{value}")

    stringified = {
      op: MessageOP::Data.to_i32,
      c: channel, d: [timestamp, value],
    }.to_json

    @subscribers[channel].each do |client_id|
      ws = @websockets[client_id]
      ws.send(stringified)
    end
  end

  private def alerts_from_str(val : Nil) : Array(Alert)
    return [] of Alert
  end

  private def alerts_from_str(alert_id_str : String) : Array(Alert)
    res = [] of Alert

    alert_id_str.split(',').each do |alert_id|
      type_id = @context.cfg["alert:#{alert_id}"]["type"]
      res.push(ALERTS[type_id].new(alert_id, @context))
    end

    return res
  end

  # Return the Alert subclass instances responsible for the service
  private def get_alerts(serv_name : String) : Array(Alert)
    alert_ids = @context.cfg["service:#{serv_name}"]["alerts"]?
    if alert_ids.nil?
      return [] of Alert
    else
      return alerts_from_str(alert_ids)
    end
  end

  def slow_latency_threshold : Int64
    @context.cfg["elstat"]["slow_threshold"].to_i64
  end

  # lat1, lat2 and lat3 are Int64 values, ordered from the oldest to newest.
  # returns the modified (or not) AlertType.
  private def latency_alert?(res : AlertType, lat1, lat2, lat3) : AlertType
    lat1 = lat1 > slow_latency_threshold
    lat2 = lat2 > slow_latency_threshold
    lat3 = lat3 > slow_latency_threshold

    @context.log.debug "L1 = #{lat1}, L2 = #{lat2}, L3 = #{lat3}"

    # the expressions are as follows (where 0 means they are below the
    # threshold, and 1 means above):
    # - to go to Slow, L1=0, L2=1, L3=1
    # - to go to SlowOk, L1=1, L2=1, L3=0

    # you may ask why we don't add the case L1=1, L2=0, L3=0, since it
    # would be a transition from Slow to SlowOk. The answer is that we
    # fetch three latencies to ignore random latency peaks brought out by
    # external factors. we need two 1s to consider any kind of transition.

    # L1 could very much be a latency peak and we wouldn't know until we
    # add in an L4 value, and I'd rather not go into this rabbit hole
    # until we fetch 300 latencies.

    if !lat1 && lat2 && lat3
      res = AlertType::Slow
    elsif lat1 && lat2 && !lat3
      res = AlertType::SlowOk
    end

    return res
  end

  private def select_alert_type(
    result : AdapterResult, serv_name : String
  ) : AlertType
    # there can be four types of alerts
    #  - error happened
    #  - error is recovered
    #  - latency is higher than slow threshold
    #  - latency is recovered
    error = result["error"]?
    lat = result["latency"].as(Int64)
    res = AlertType::None

    # for alerts, we need to fetch the last three status+latency rows
    # off the service.

    # NOTE this will crash with the adapter that doesn't have status
    # and latency, lol!

    rows = @context.db.query_all(
      "select timestamp, status, latency
      from #{serv_name}
      order by timestamp desc
      limit 3", as: {Int64, Bool, Int64})

    if rows.size < 3
      return AlertType::None
    end

    # the order here isn't very intuitive, but this is required.
    # last contains the most recent one (yes, that's also not very intuitive,
    # considering we already have the result variable)
    last = rows[0]
    first_1 = rows[1]
    first_0 = rows[2]

    # the status checking happens on the last two rows
    status_1 = first_1[1]
    status_2 = last[1]

    # values for alert type:
    # -> Error: S1 = 0, S2 = 1
    # -> ErrorOk: S1 = 1, S2 = 0

    @context.log.debug "S1 = #{status_1}, S2 = #{status_2}"

    if !status_1 && status_2
      res = AlertType::ErrorOk
    elsif status_1 && !status_2
      res = AlertType::Error
    else
      res = AlertType::None
    end

    # check if we need to do Slow or SlowOk, this uses all three last latency
    # values. also latency_alert?() is called last because it has a higher
    # priority and so can overwrite ErrorOk alerts.

    if res == AlertType::Error
      return res
    else
      res = latency_alert?(res, first_0[2], first_1[2], last[2])
      return res
    end
  end

  private def do_alert(
    alert : Alert, alert_type : AlertType,
    serv_name : String, result : AdapterResult
  )
    case alert_type
    when AlertType::None
      return
    when AlertType::Error
      alert.alert_err serv_name, result
    when AlertType::ErrorOk
      alert.alert_err_ok serv_name, result
    when AlertType::Slow
      alert.alert_slow serv_name, result
    when AlertType::SlowOk
      alert.alert_slow_ok serv_name, result
    end
  end

  private def check_alerts(serv_name : String)
    result = @state[serv_name]

    service_alerts = self.get_alerts(serv_name)

    # select which type of alert to do, based on checks
    alert_type = self.select_alert_type(result, serv_name)

    if alert_type != AlertType::None
      @context.log.warn("service #{serv_name} alert : #{alert_type}")
    end

    service_alerts.each do |alert|
      spawn do_alert(alert, alert_type, serv_name, result)
    end
  end

  # Waits for the service workers to send data
  # through the main channel. This fiber is the only one
  # doing a receive() on @main_channel.
  private def fiber_main
    while true
      value = @main_channel.receive
      @context.log.debug("got value: #{value}")

      serv_name = value[0]
      result = value[1]

      error = result["error"]?

      if !error.nil?
        result["status"] = false
        result["latency"] = 0
      end

      # regardless of the error state, we'll commit it to the database
      # for proper uptime calculations. the actual error isn't commited due
      # to the "error" field not being in the adapter's spec.db (which is good)
      timestamp = self.commit_result(serv_name, result)
      self.dispatch_result(serv_name, timestamp, result)

      @state[serv_name] = {
        "timestamp" => timestamp,
      }.merge(result)

      self.check_alerts(serv_name)
    end
  end

  # We only create a websocket channel when the given
  # prefix is in the service adapter's spec.
  private def check(columns : Array(String), prefix : String, name : String)
    if columns.includes?(prefix)
      ws_chan_name = "#{prefix}:#{name}"
      @subscribers[ws_chan_name] = Set(WebsocketID).new
      @context.log.info("Created channel '#{ws_chan_name}'")
    end
  end

  private def ws_channel_setup(service : Service, name : String)
    columns = column_names(service)

    # TODO: hardcode status and latency as being the only possible
    # websocket channels somewhere.
    self.check(columns, "status", name)
    self.check(columns, "latency", name)
  end

  # initialize the manager with its channels
  # and tables
  def setup
    @subscribers["incidents"] = Set(WebsocketID).new

    spawn fiber_main()

    # for each service we need to:
    # - Create tables for it
    # - Create websocket channels
    # - Start a service worker, giving it @main_channel
    self.service_keys.each do |service_key|
      name = service_key.lchop("service:")
      service = @context.cfg[service_key]

      self.make_db_table(name, service)
      self.ws_channel_setup(service, name)

      worker = ServiceWorker.new(name, service, @main_channel, @context)
      spawn worker.run

      # the worker can take any time to start up, so the
      # manager must fill in data to not crash if
      # a client requests /api/status before worker replies
      res = AdapterResult.new
      res["status"] = false
      res["timestamp"] = 0.to_i64
      @state[name] = res
    end
  end

  # Subscribe the given websocket (with its client ID) to a list of channels.
  # Registers the websocket's Client ID into the internal websocket mapping.
  # Returns the list of channels that the client successfully subscribed
  # to.
  def subscribe(ws, client_id, channels : Array(String)) : Array(String)
    @websockets[client_id] = ws
    subscribed = [] of String

    channels.each do |channel|
      subs = @subscribers[channel]?
      if !subs.nil?
        subs.add(client_id)
        subscribed.push(channel)
      end
    end

    @context.log.info("subscribed #{client_id} to #{subscribed}")
    return subscribed
  end

  # Unsubscribe the given WebsocketID from the given list of channels.
  def unsubscribe(
    client_id : WebsocketID, channels : Array(String)
  ) : Array(String)
    unsubbed = [] of String

    channels.each do |channel|
      subs = @subscribers[channel]?

      if (!subs.nil?) && subs.delete(client_id)
        unsubbed.push(channel)
      end
    end

    @context.log.info("unsubbed #{client_id} from #{unsubbed}")
    return unsubbed
  end

  # Remove the given WebsocketID from all channels.
  def rm_websocket(client_id : WebsocketID) : Array(String)
    unsubbed = unsubscribe(client_id, @subscribers.keys)

    # We remove the client id from @websockets AFTER the unsubscribe()
    # call to not cause havoc considering concurrency.
    @websockets.delete(client_id)
    return unsubbed
  end

  def fetch_stages(incident_id : UUID) : Array(IncidentStage)
    res = [] of IncidentStage

    rows = @context.db.query_all(
      "select timestamp, title, content
      from incident_stages
      where parent_id = ?
      order by timestamp asc", incident_id.to_s, as: {Int64, String, String})

    rows.each do |row|
      res.push(IncidentStage.new(*row))
    end

    return res
  end

  # Fetch a single incident.
  def fetch_incident(incident_id : UUID) : Incident?
    incident = @context.db.query_one?(
      "select id, incident_type, title, content, ongoing, start_timestamp, end_timestamp
      from incidents
      where id = ?", incident_id.to_s, as: {String, String, String, String, Bool, Int64, Int64?})

    if incident.nil?
      return nil
    end

    incident = Incident.new(*incident)
    incident.stages = fetch_stages(incident_id)

    return incident
  end

  # Publish a given opcode
  def dispatch_incident(opcode : MessageOP, data : Incident)
    case opcode
    when MessageOP::IncidentNew,
         MessageOP::IncidentUpdate,
         MessageOP::IncidentClose
      spawn do
        stringified = {
          op: opcode.to_i32,
          c:  "incidents",
          d:  data.to_map,
        }.to_json
        @subscribers["incidents"].each do |client_id|
          ws = @websockets[client_id]
          ws.send(stringified)
        end
      end
    else
      raise "invalid op code"
    end
  end

  def alert_incident_one(inc_alert_type, incident_id, alert)
    incident = fetch_incident incident_id

    if incident.nil?
      @context.log.warn "incident given to alert is nil"
      return
    end

    case inc_alert_type
    when IncidentAlertType::Create
      alert.alert_incident_new incident
    when IncidentAlertType::Close
      alert.alert_incident_close incident
    when IncidentAlertType::NewStage
      alert.alert_incident_new_stage incident
    end
  end

  # Make an alert for a given incident.
  def alert_incident(inc_alert_type : IncidentAlertType, incident_id : UUID)
    alerts = alerts_from_str(@context.cfg["elstat"]["incident_alert"]?)

    alerts.each do |alert|
      spawn do
        alert_incident_one(inc_alert_type, incident_id, alert)
      end
    end
  end
end
