require "kemal"
require "logger"
require "signal"

require "./context"
require "./manager"
require "./config_checker"

require "./api/main"
require "./api/streaming"
require "./api/incidents"

ctx = Context.new
check_config(ctx.cfg)
ctx.setup

manager = Manager.new(ctx)
manager.setup

public_folder "./priv/frontend/build"

static_headers do |response, filepath, filestat|
  if filepath =~ /\.html$/
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add("Access-Control-Allow-Headers", "*")
    response.headers.add("Access-Control-Allow-Methods", "*")
  end

  response.headers.add("Content-Size", filestat.size.to_s)
end

add_context_storage_type(Manager)
add_context_storage_type(Logger)

before_all "*" do |env|
  env.set "manager", manager
  env.set "log", manager.context.log
  env.response.content_type = "application/json"
end

get "/awoo" do
  raise "uwu"
end

error 500 do |env, err|
  {error:   true,
   code:    500,
   message: "#{err}"}.to_json
end

error 404 do |env, err|
  {error:   true,
   code:    404,
   message: "#{err}"}.to_json
end

get "/" do |env|
  send_file env, "./priv/frontend/build/index.html"
end

# The websocket entrypoint must be here because before_all
# does not apply to websocket handlers.
ws "/api/streaming" do |ws, env|
  websocket_start(ws, env, manager)
end

Kemal.run do
  Signal::INT.trap do
    log "shutting down"
    ctx.db.close
    Kemal.stop
    exit
  end
end
