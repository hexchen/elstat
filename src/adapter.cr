require "http"
require "json"
require "./context"
require "./manager"
require "./status_codes"

alias AdapterValue = (String | Int64 | Bool)
alias AdapterResult = Hash(String, AdapterValue)

class AdapterError < Exception
end

class Adapter
  def self.spec
    raise "adapter has no spec"
  end

  def self.query(ctx : Context, serv_cfg : Service) : AdapterResult
    raise "not implemented"
  end

  private def self.construct(res_array : Array(AdapterValue)) : AdapterResult
    spec = self.spec
    cols = spec[:db][1, spec.size + 1]
    res = AdapterResult.new

    cols.map_with_index do |column, idx|
      res[column] = res_array[idx]
    end

    return res
  end
end

class PingAdapter < Adapter
  @@PING_RGX = /(.+)( 0% packet loss)(.*)/
  @@PING_LATENCY_RGX = /time\=(\d+(\.\d+)?) ms/
  @@PING_ERROR_RGX = /icmp_seq\=(\d+)\ ([^time].*)$'/

  def self.spec
    {db: ["timestamp", "status", "latency"]}
  end

  def self.query(ctx, serv_cfg)
    stdout = IO::Memory.new

    proc = Process.new(
      "ping",
      args: ["-c", "1", serv_cfg["ping_addr"]],
      output: stdout,
      error: stdout)

    status = proc.wait
    buffer = stdout.to_s

    # TODO check status code for alive check.
    alive = !@@PING_RGX.match(buffer).nil?
    latency = @@PING_LATENCY_RGX.match(buffer)

    if latency.nil?
      latency = 0.to_i64
    else
      lat_i64 = latency[1].to_i64?
      lat_f64 = latency[1].to_f64?

      if lat_i64.nil? && lat_f64.nil?
        latency = 0.to_i64
      elsif lat_i64.nil? && !lat_f64.nil?
        latency = Math.max(lat_f64, 1.to_f64)
      elsif !lat_i64.nil?
        latency = lat_i64
      else
        latency = 0.to_i64
      end
    end

    if latency.is_a?(Float64)
      latency = latency.to_i64
    end

    # At this point, latency is UInt64.

    if alive
      return self.construct [alive, latency]
    else
      # extract error data and raise
      err = @@PING_ERROR_RGX.match(buffer)

      if !alive && !err.nil?
        err = err[2]
      else
        err = "packet lost"
      end

      raise AdapterError.new(err)
    end
  end
end

class HTTPAdapter < Adapter
  def self.spec
    {db: ["timestamp", "status", "latency"]}
  end

  # Get a human-friendly phrase describing the error code.
  def self.get_phrase(status_code : String) : String
    text = STATUS[status_code]?
    return text || "Unknown status"
  end

  def self.query(ctx, serv_cfg)
    begin
      start = Time.monotonic
      resp = HTTP::Client.get(serv_cfg["http_url"])
      elapsed = (Time.monotonic - start).total_milliseconds.to_i64
      status = resp.status_code

      success = status == 200 || status == 204
      # success = false

      if !success
        elapsed = 0.to_f64
        status_phrase = self.get_phrase(status.to_s)
        raise AdapterError.new("http status #{status} - #{status_phrase}")
      end

      return self.construct [success, elapsed]
    rescue
      raise AdapterError.new("http error")
    end
  end
end
