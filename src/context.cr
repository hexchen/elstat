require "ini"
require "logger"
require "db"
require "sqlite3"

class Context
  property cfg = INI.parse(File.read("config.ini"))
  property log = Logger.new(STDOUT)
  property db : DB::Database = DB.open("sqlite3://elstat.db")

  def setup
    root = cfg["elstat"]
    if root["debug"]? == "1"
      log.info("setting logger to debug")
      log.level = Logger::DEBUG
    end
  end
end
