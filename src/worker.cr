require "./manager"
require "./adapter"
require "./context"

alias WorkerResult = Tuple(String, AdapterResult)

class ServiceWorker
  property name : String
  property service : Service
  property channel : Channel(WorkerResult)
  property ctx : Context

  def initialize(@name, @service, @channel, @ctx)
  end

  def run
    @ctx.log.info("worker start #{@name}")

    adapter_name = service["adapter"]
    adapter = ADAPTERS[adapter_name]

    sleep_secs = @service["poll"].to_u32
    loop do
      begin
        result = adapter.query(@ctx, service)
        @channel.send({@name, result})
      rescue ex : AdapterError
        @ctx.log.warn("adapter err '#{name}': '#{ex}'")

        # create own AdapterResult containing the error message
        res = AdapterResult.new
        res["status"] = false
        res["latency"] = 0
        res["error"] = ex.message || "Error not found"
        @channel.send({@name, res})
      ensure
        sleep sleep_secs
      end
    end
  end
end
