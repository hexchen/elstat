require "./alerts"
require "twilio"

class TwilioAlert < Alert
  @client : Twilio::Client
  @numbers : Array(String)
  @outgoing : String
  @call_url : String

  def initialize(@alert_name : String, @ctx : Context)
    account_sid = @ctx.cfg["alert:#{@alert_name}"]["sid"]
    auth_token = @ctx.cfg["alert:#{@alert_name}"]["token"]
    @client = Twilio::Client.new(account_sid, auth_token)
    @numbers = @ctx.cfg["alert:#{@alert_name}"]["numbers"].split(",")
    @outgoing = @ctx.cfg["alert:#{@alert_name}"]["outgoing"]
    @call_url = @ctx.cfg["alert:#{@alert_name}"]["call_url"]
  end

  def alert_err(serv_name : String, result : AdapterResult)
    @numbers.each do |number|
      if @call_url != "false"
        @client.calls.create(
          from: @outgoing,
          to: number,
          url: @call_url
        )
      end
      @client.messages.create(
        from: @outgoing,
        to: number,
        body: "#{serv_name} is non-responsive (got #{result["error"]}), please check."
      )
    end
  end

  def alert_err_ok(serv_name : String, result : AdapterResult)
    downtime = calc_downtime serv_name, result
    @numbers.each do |number|
      @client.messages.create(
        from: @outgoing,
        to: number,
        body: "Issue with #{serv_name} has been resolved after #{downtime.total_minutes.round(1)}m."
      )
    end
  end

  def alert_slow(serv_name : String, result : AdapterResult)
    @numbers.each do |number|
      @client.messages.create(
        from: @outgoing,
        to: number,
        body: "#{serv_name} is slow (got #{result["latency"]})"
      )
    end
  end

  def alert_slow_ok(serv_name : String, result : AdapterResult)
  end

  def alert_incident_new(incident : Incident)
    @numbers.each do |number|
      @client.messages.create(
        from: @outgoing,
        to: number,
        body: "Opened [#{incident.incident_type}]: #{incident.title}"
      )
    end
  end

  def alert_incident_close(incident : Incident)
    @numbers.each do |number|
      @client.messages.create(
        from: @outgoing,
        to: number,
        body: "Closed [#{incident.incident_type}]: #{incident.title}"
      )
    end
  end

  def alert_incident_new_stage(incident : Incident)
  end
end
