require "../context"
require "../adapter"
require "../api/incidents"

require "time"

# Main superclass for alerts.
abstract class Alert
  def initialize(@alert_name : String, @ctx : Context)
  end

  abstract def alert_err(serv_name : String, result : AdapterResult)
  abstract def alert_err_ok(serv_name : String, result : AdapterResult)
  abstract def alert_slow(serv_name : String, result : AdapterResult)
  abstract def alert_slow_ok(serv_name : String, result : AdapterResult)
  abstract def alert_incident_new(incident : Incident)
  abstract def alert_incident_close(incident : Incident)
  abstract def alert_incident_new_stage(incident : Incident)

  def calc_downtime(serv_name : String, result) : Time::Span
    # find the last result that worked before the failures
    ts_success = @ctx.db.scalar("
      select timestamp from #{serv_name}
      where timestamp < ? and status = 1
      order by timestamp desc
      limit 1
    ", result["timestamp"])

    # count all the downtime that happened since then
    # we do this by counting all the results that failed, then
    # multiplying that by the currently set poll time on the service
    down_hits = @ctx.db.scalar(
      "select count(*) from #{serv_name}
       where timestamp > ? and status = 0", ts_success).as(Int64)

    Time::Span.new(
      seconds: down_hits * @ctx.cfg["service:#{serv_name}"]["poll"].to_i64,
      nanoseconds: 0,
    )
  end
end
