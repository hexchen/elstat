import React from 'react'
import { shallow } from 'enzyme'

import DegradedNotice from '../DegradedNotice'

it('renders without crashing', () => {
  shallow(<DegradedNotice services={{ first: null, second: null }} />)
})

it('shows one service', () => {
  const comp = shallow(<DegradedNotice services={{ first: null }} />)
  expect(comp.contains(<header>1 service is unreachable</header>)).toEqual(true)
  expect(
    comp.contains(
      <p>
        elstat is having trouble contacting <strong>first</strong>.
      </p>
    )
  ).toEqual(true)
})

it('shows multiple services', () => {
  const comp = shallow(
    <DegradedNotice services={{ first: null, second: null, third: null }} />
  )
  expect(comp.contains(<header>3 services are unreachable</header>)).toEqual(
    true
  )
  expect(
    comp.contains(
      <p>
        elstat is having trouble contacting{' '}
        <strong>first, second, third</strong>.
      </p>
    )
  ).toEqual(true)
})
