import React from 'react'

import ReactPlaceholder from 'react-placeholder'
import 'react-placeholder/lib/reactPlaceholder.css'

import './ServicePlaceholder.css'

export default function ServicePlaceholder() {
  return (
    <div className="service-placeholder">
      <div className="title">
        <ReactPlaceholder
          type="round"
          ready={false}
          showLoadingAnimation
          className="emoji"
        >
          {' '}
        </ReactPlaceholder>
        <ReactPlaceholder
          type="rect"
          ready={false}
          showLoadingAnimation
          className="name"
        >
          {' '}
        </ReactPlaceholder>
      </div>
      <ReactPlaceholder
        type="text"
        ready={false}
        rows={1}
        showLoadingAnimation
        className="text"
      >
        {' '}
      </ReactPlaceholder>
      <ReactPlaceholder
        type="rect"
        ready={false}
        showLoadingAnimation
        className="graph"
      >
        {' '}
      </ReactPlaceholder>
    </div>
  )
}
