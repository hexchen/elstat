require "json"
require "./spec_helper"

describe "Elstat" do
  it "renders /" do
    get "/"
    response.status_code.should eq 200
  end

  # TODO: we need to somehow add some test services
  # to check anything that is above slow_threshold.

  it "can request quick status" do
    get "/api/quick"
    response.status_code.should eq 200
    rjson = JSON.parse response.body
    rjson["slow_threshold"].as_i?.nil?.should eq false
  end

  it "can request full status" do
    get "/api/status"
    response.status_code.should eq 200

    # TODO: graph checking
    rjson = JSON.parse response.body
  end
end
